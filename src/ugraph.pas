unit ugraph;

{$I fpc.inc}

interface

uses
    Classes
  , SysUtils
  , umineContainer
  , umine
  , uOperation
  , Types
  ;

type

  TProcOfObject = procedure of object;
  PNode = ^TNode;

  PEdge = ^TEdge;
  TEdge = record
    nfrom,
    nto: PNode;
    op: TOperations;
    weight: Integer;
    cmd: TMovement;
  end;

  TNode = record
    parent: PEdge;
    children: Array[TMovement] of PEdge;
    done: Boolean;

    // states.
    fRobotx,
    fRoboty: Integer;
    fLiftx,
    fLifty: Integer;
    fLiftOpened: Boolean;
    fLambdaCount: Integer;
    fScore: Integer;
    fSteps: Integer;


    fWater: Integer;
    fFlooding: Integer;
    fWaterproof: Integer;
    fUnderWater: integer;
    fDone: Boolean;

    fGrowthVar: Integer;
    fRazors: Integer;

    lambda: Array of TPoint;
  end;


  TGraph = class
  private
    fState: PNode;
    fRoot: TNode;

    fMine: TMine;
    fOnMoved: TProcOfObject;

    function NewPNode: PNode;
  protected
    procedure Node2Mine( ln: PNode );
    procedure Mine2Node( ln: PNode );
  public
    constructor Create( lMine: TMine );
    destructor Destroy;

    function EndEdge( e: PEdge ): Boolean;
    function GetPath: String;

    function DoMove( m: TMovement ): TMoveResult;
    procedure Undo;

    property Root: TNode read fRoot;
    property State: PNode read FState;
    property Mine: TMine read fMine;

    property OnMoved: TProcOfObject read fOnMoved write fOnMoved;
  end;

implementation

const
  cDefEdge: TEdge = ( nfrom : nil; nto : nil; op : nil );

{ TGraph }

constructor TGraph.Create( lMine: TMine );
var
  m: TMovement;
begin
  fMine := lMine;

  fRoot.parent := nil;
  for m := Low( TMovement ) to High( TMovement ) do
    fRoot.children[m] := nil;

  Mine2Node( @fRoot );
  fState := @Root;
end;

destructor TGraph.Destroy;
begin
  // destroy.
  inherited;
end;

function TGraph.NewPNode: PNode;
var
  m: TMovement;
begin
  New( result );
  result^.parent := nil;
  for m := Low( TMovement ) to High( TMovement ) do
    result^.children[m] := nil;
  result^.lambda := nil;
end;

function TGraph.DoMove( m: TMovement ): TMoveResult;
var
  i: Integer;
begin
  result := mrNone;
  if EndEdge( State^.children[m] ) then
    exit;

  if State^.children[m] <> nil then
  begin

    State^.children[m]^.op.Apply( fMine );
    Node2Mine( State^.children[m]^.nto );
    // Already calculated.
  end else
  begin
    New( State^.children[m] );
    State^.children[m]^.nfrom := State;

    if not fMine.canMove( m ) then
    begin
      State^.children[m]^.nto := nil;
      State^.children[m]^.op := nil;
      // ide be szabad jonnie, csak nem aikor a megoldast keresi.
    end;

    State^.children[m]^.nto := NewPNode;
    State^.children[m]^.nto^.parent := State^.children[m];
    State^.children[m]^.op := TOperations.Create;
    State^.children[m]^.cmd := m;
    fMine.OnPixelChanging := {$ifdef fpc}@{$endif}State^.children[m]^.op.Add2;
    Result := fMine.DoMove( m );
    fMine.OnPixelChanging := nil;

    Mine2Node( State^.children[m]^.nto );
    fState := State^.children[m]^.nto;
    fState^.done := fMine.Done;
  end;

  if Assigned( fOnMoved ) then fOnMoved;
end;

function TGraph.GetPath: String;
var
  pn: PNode;
begin
  pn := State;
  result := '';
  while pn^.parent <> nil do
  begin
    result :=  cMovements[pn^.parent^.cmd] + result;
    pn := pn^.Parent^.nfrom;
  end;
end;

procedure TGraph.Undo;
begin
  if State^.parent = nil then
    exit;

  State^.parent^.op.Revert( fMine );
  fState := State^.parent^.nfrom;
  Node2Mine( fState );
  if Assigned( fOnMoved ) then fOnMoved;
end;


procedure TGraph.Mine2Node(ln: PNode);
var
  i: Integer;
begin
  ln^.fRobotx := fMine.Robotx;
  ln^.fRoboty := fMine.Roboty;
  ln^.fLiftx := fMine.Liftx;
  ln^.fLifty := fMine.Lifty;
  ln^.fLiftOpened := fMine.LiftOpened;
  ln^.fLambdaCount := fMine.LambdaCount;
  ln^.fScore := fMine.Score;
  ln^.fSteps := fMine.Steps;

  ln^.fWater := fMine.Water;
  ln^.fFlooding := fMine.Flooding;
  ln^.fWaterproof := fMine.Waterproof;
  ln^.fUnderWater := fMine.UnderWater;
  ln^.fDone := fMine.Done;

  ln^.fGrowthVar := fMine.GrowthVar;
  ln^.fRazors := fMine.Razors;

  if ln^.lambda = nil then
    SetLength( ln^.lambda, ln^.fLambdaCount );
  for i := 0 to ln^.fLambdaCount - 1 do
    ln^.lambda[i] := fMine.Lambda[i];
end;


procedure TGraph.Node2Mine( ln: PNode );
var
  i: Integer;
begin
  fMine.Robotx := ln^.fRobotx;
  fMine.Roboty := ln^.fRoboty;
  fMine.Liftx := ln^.fLiftx;
  fMine.Lifty := ln^.fLifty;
  fMine.LiftOpened := ln^.fLiftOpened;
  fMine.LambdaCount := ln^.fLambdaCount;
  fMine.Score := ln^.fScore;
  fMine.Steps := ln^.fSteps;

  fMine.Water := ln^.fWater;
  fMine.Flooding := ln^.fFlooding;
  fMine.Waterproof := ln^.fWaterproof;
  fMine.UnderWater := ln^.fUnderWater;
  fMine.Done := ln^.fDone;

  fMine.GrowthVar := ln^.fGrowthVar;
  fMine.Razors := ln^.fRazors;

  for i := 0 to ln^.fLambdaCount - 1 do
    fMine.Lambda[i] := ln^.lambda[i];

end;

function TGraph.EndEdge( e: PEdge ): Boolean;
begin
  result := ( e <> nil ) and ( e^.nto = nil );
end;
end.
