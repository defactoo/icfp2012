unit umine;

{$I fpc.inc}

interface

uses
    Classes
  , Types
  , umineContainer
  , uoperation

  ;

type
  TMoveResult = ( mrNone, mrAborted, mrSuccess, mrDied );

  TTrampolinRel = record
    trampolin: TMinePixel;
    target: TMinePixel;
  end;

  TTrampolinRelA = Array of TTrampolinRel;

  TOnNotify = procedure of Object;

  TEndInfo = ( eiNotYet, eiWin, eiAbort, eiDieRock, eiDieWater );

  TMine = class( TMineContainer )
  private
    fEndInfo: TEndInfo;
    fRobotx,
    fRoboty: Integer;
    fLiftx,
    fLifty: Integer;
    fLiftOpened: Boolean;
    fLambdaCount: Integer;
    fScore: Integer;

    fWater: Integer;
    fFlooding: Integer;
    fWaterproof: Integer;
    fUnderWater: integer;

    fGrowth: Integer;
    fGrowthVar: Integer;
    fRazors: Integer;

    // seged.
    fSteps: Integer;
    fInitialLambdaCount: Integer;
    TrampolinRel: TTrampolinRelA;

    //events
    fOnWaterChanged: TOnNotify;
    fDone: Boolean;
    procedure setLambda(indx: Integer; const Value: TPoint);
    function getLambda(indx: Integer): TPoint;

  protected
    fLambdas: Array of TPoint;
    fLambdasCount: Integer; // temporary for uploading. need

    procedure AddLambda( x,y: Integer );
  public


    constructor Create( tsl: TStrings );
    destructor Destroy; override;

    function canMove( mm: TMovement ): Boolean;
    function DoMove( mm: TMovement ): TMoveResult;

    function AreThereMovingStones: Boolean;
    function StoneCanFall( i, j: Integer ): Boolean;
    function StoneCanbeMoved( x, y: Integer ): Boolean;
    function PossibleGoThere( x, y: Integer ): Boolean;


    function UnderWaterLambdaCount: Integer;
    procedure PrintState;

    property Robotx: Integer read fRobotx write fRobotx;
    property Roboty: Integer read fRoboty write fRoboty;

    property Liftx: Integer read fLiftx write fLiftx;
    property Lifty: Integer read fLifty write fLifty;
    property LiftOpened: Boolean read fLiftOpened write fLiftOpened;

    property LambdaCount: Integer read fLambdaCount write fLambdaCount;
    property Score: Integer read fScore write fScore;

    property Water: Integer read fWater write fWater;
    property Flooding: Integer read fFlooding write fFlooding;
    property Waterproof: Integer read fWaterproof write fWaterproof;
    property UnderWater: integer read fUnderWater write fUnderWater;

    property GrowthVar: Integer read fGrowthVar write fGrowthVar;
    property Razors: Integer read fRazors write fRazors;


    property Done: Boolean read fDone write fDone;
    property EndInfo: TEndInfo read fEndInfo;
    // seged.
    property InitialLambdaCount: Integer read fInitialLambdaCount;

    property Steps: Integer read fSteps write fSteps;

    property Lambda[indx: Integer]: TPoint read getLambda write setLambda;

    // for the gui
    property OnWaterChanged: TOnNotify read fOnWaterChanged write fOnWaterChanged;

  end;

const
  cEndInfoStr : Array[TEndInfo] of String = (
    'Not yet...',
    'WIN',
    'ABORT',
    'DIE(rock)',
    'DIE(water)'
  );
implementation

uses
    SysUtils
  , Math  
  , uHelper
  ;

{ TMine }

constructor TMine.Create( tsl: TStrings );
var
  i,j,k: Integer;

  meta,
  tmp: TStringList;
  smeta: String;
begin
  fEndInfo := eiNotYet;
  fLambdaCount := 0;
  fScore := 0;
  fDone := false;

  fWater := 0;
  fFlooding := 0;
  fWaterProof := 10;
  fUnderWater := 0;

  fGrowth := 25;
  fGrowthVar := 25 - 1;
  fRazors := 0;

  // looking for metadata
  i := 0;
  while ( i < tsl.Count ) and ( Trim( tsl[i] ) <> '' ) do
    inc( i );

  try
    if i < tsl.Count then
    begin
      k := 0;
      SetLength( TrampolinRel, tsl.Count - i );
    end;

    meta := TStringList.Create;
    meta.NameValueSeparator := ' ';
    while ( i < tsl.Count ) do
    begin
      smeta := Trim( tsl[i] );
      if smeta <> '' then
        meta.Add( smeta );
      tsl.delete( i );
    end;

    for i := 0 to meta.Count - 1 do
    begin
      if meta.Names[i] = 'Water' then
        fWater := StrToInt( meta.ValueFromIndex[i] ) else
      if meta.Names[i] = 'Flooding' then
        fFlooding := StrToInt( meta.ValueFromIndex[i] ) else
      if meta.Names[i] = 'Waterproof' then
        fWaterproof := StrToInt( meta.ValueFromIndex[i] ) else
      if meta.Names[i] = 'Trampoline' then
      begin
        tmp := TStringList.Create;
        try
          tmp.Delimiter := ' ';
          tmp.DelimitedText := meta.ValueFromIndex[i];
          TrampolinRel[k].trampolin := CharToMinePixl(tmp[0][1]);
          TrampolinRel[k].target := CharToMinePixl(tmp[2][1]);
          Inc( k );
        finally
          tmp.Free;
        end;
      end else
      if meta.Names[i] = 'Growth' then
      begin
        fGrowth := StrToInt( meta.ValueFromIndex[i] );
        fGrowthVar := fGrowth - 1;
      end else
      if meta.Names[i] = 'Razors' then
        fRazors := StrToInt( meta.ValueFromIndex[i] ) else

    end;
  finally
    meta.Free;
  end;
  SetLength( TrampolinRel, k );
  // metadata looking end.

  inherited;
  
  for j := 1 to m do
  for i := 1 to n do
  begin
    if pixel[i,j] = mpRobot then
    begin
      fRobotx := i;
      fRoboty := j;
    end else

    if pixel[i,j] = mpLambda then
    begin
      inc( fLambdaCount )
    end else
    if pixel[i,j] = mpHighOrderRock then
    begin
      inc( fLambdaCount )
    end else
    if ( pixel[i,j] = mpClosedLift ) or
       ( pixel[i,j] = mpOpenedLift ) then
    begin
      fLiftx := i;
      fLifty := j;
      fLiftOpened := pixel[i,j] = mpOpenedLift;
    end;

  end;

  fInitialLambdaCount := fLambdaCount;

  SetLength( fLambdas, InitialLambdaCount );
  for j := 1 to m do
  for i := 1 to n do
  if pixel[i,j] = mpLambda then
    AddLambda( i, j );

end;

destructor TMine.Destroy;
begin

  inherited;
end;

procedure TMine.PrintState;
var
  i,j: Integer;
begin
  writeln;

  writeln( 'lambda=' + IntToStr( InitialLambdaCount - LambdaCount ) + '/' +
    IntToStr( InitialLambdaCount ) + ', ' +
    'moves=' + IntToStr( Steps ) + ', ' +
    'water=' + IntToStr( Water ) + ' (' +  IntToStr( UnderWater ) + '), ' +
    'razor=' + IntToStr( Razors )
  );

  for j := m downto 1 do
  begin
    for i := 1 to n do
      Write( cMinePixelStr[pixel[i,j]] );
    writeln;
  end;

end;

function TMine.canMove( mm: TMovement ): Boolean;
var
  dx,dy,
  nx,ny,
  nx2,ny2,
  i,j: Integer;
  np: TMinePixel;
begin
  if ( mm = mWait ) or ( mm = mAbort ) then
    result := True
  else
  if mm = mShave Then
    result := False
  else begin
    dx := 0;
    dy := 0;
    case mm of
      mLeft: dx := -1;
      mRight: dx := 1;
      mUp: dy := 1;
      mDown: dy := -1;
    end;
    nx := Robotx + dx;
    ny := Roboty + dy;

    result := ValidIndex( nx, ny );
    if not result then
      exit;

    np := pixel[nx,ny];

    if ( np = mpWall ) or ( np = mpClosedLift ) or ( np = mpBeard ) or
       ( ( np >= mpTarget1 ) and ( np <= mpTarget9 ) ) then
    begin
      result := false;
      exit;
    end;

    if ( np = mpLambda ) or ( np = mpEmpty ) or
       ( np = mpOpenedLift ) or ( np = mpEarth ) or

       // Trampoline condition
       ( ( np >= mpTrampolinA ) and ( np <= mpTrampolinI ) ) or

       // Beard - Razor Condition
       ( np = mpRazor ) then
    begin
      result := True;
      exit;
    end else

    if ( np = mpRock ) or ( np = mpHighOrderRock ) then
    begin
      nx2 := Robotx + dx * 2;
      ny2 := Roboty + dy * 2;
      result := ValidIndex( nx2, ny2 ) and ( dy = 0 ) and
        ( pixel[nx2,ny2] = mpEmpty );
    end else
      raise EMineException.Create( '[TMine.canMove]: Unknown object: ' + cMinePixelStr[np] );

  end;
end;

function TMine.DoMove( mm: TMovement ): TMoveResult;
var
  i,j,k,l,
  dx,dy,
  nx,ny,
  nx2,ny2: Integer;
  np,tr: TMinePixel;
  trl: TMinePixelList;

  bWasEmpty: Boolean; // for checking death by rock.
  ops: TOperations;
begin
  Inc( fSteps );
  Dec( fScore );
  if mm = mWait then
  begin
    //nothing.
  end else

  if mm = mShave then
  begin
    if Razors > 0 then
    for k := Roboty - 1 to Roboty + 1 do
    for l := Robotx - 1 to Robotx + 1 do
    if ValidIndex( l,k ) and ( Pixel[ l,k ] = mpBeard ) then
      setPixel( l,k, mpEmpty );
    dec( fRazors );
  end else

  if canMove( mm ) then
  begin
    dx := 0;
    dy := 0;
    case mm of
      mLeft: dx := -1;
      mRight: dx := 1;
      mUp: dy := 1;
      mDown: dy := -1;
      //mWait:
      mAbort:
      begin
        Inc( fScore );
        result := mrAborted;
        fEndInfo := eiAbort;
        fScore := fScore + ( fInitialLambdaCount - LambdaCount ) * 25;
        fDone := True;
        exit;
      end;
    end;

    // new position, and pixel
    nx := Robotx + dx;
    ny := Roboty + dy;
    np := Pixel[ nx, ny ];

    if np = mpLambda then
    begin
      Dec( fLambdaCount );
      Inc( fScore, 25 );
      setPixel( nx, ny, mpRobot );
    end else

    if np = mpOpenedLift then
    begin
      fEndInfo := eiWin;
      Result := mrSuccess;
      fDone := True;
      Inc( fScore, fInitialLambdaCount * 50 );
      setPixel( nx, ny, mpRobot );
      setPixel( Robotx, Roboty, mpEmpty );
      exit;
    end else

    if np = mpRazor then
    begin
      Inc( fRazors );
      setPixel( nx, ny, mpRobot );
    end;

    if ( np = mpEarth ) or ( np = mpEmpty ) then
    begin
      setPixel( nx, ny, mpRobot );
    end else

    // Rock.
    if ( np = mpRock ) or ( np = mpHighOrderRock ) then
    begin
      nx2 := Robotx + dx * 2;
      ny2 := Roboty + dy * 2;
      setPixel( nx2, ny2, np );
      setPixel( nx, ny, mpRobot );
    end else


    if ( np >= mpTrampolinA ) and ( np <= mpTrampolinI ) then
    begin
      // looking for Trampolin target
      k := 0;
      while ( k < Length( TrampolinRel ) ) and
        ( TrampolinRel[k].trampolin <> np ) do
        inc( k );

      if k = Length( TrampolinRel ) then
        raise EMineException.Create( 'No target for Trampolin: ' + cMinePixelStr[np] );

      tr := TrampolinRel[k].target;

      trl := TMinePixelList.Create;
      try
        // collect connected Trampolins to target.
        for k := 0 to Length( TrampolinRel ) - 1 do
        if TrampolinRel[k].target = tr then
          trl.Add( TrampolinRel[k].trampolin );

        // Clear trampolins
        for j := 1 to m do
        for i := 1 to n do
        begin
          if pixel[i,j] = tr then
          begin
            // New robot positions.
            setPixel(i,j, mpRobot );
            nx := i;
            ny := j;
          end else begin
            k := 0;
            while ( k < trl.Count ) and ( pixel[i,j] <> trl.Pixel[k] ) do
              inc( k );
            if k < trl.Count then
              setPixel( i, j, mpEmpty );
          end;
        end;

      finally
        trl.Free;
      end;

    end;

    // Clear the robot
    setPixel( Robotx, Roboty, mpEmpty );
    // set new robot pos.
    fRobotx := nx;
    fRoboty := ny;

  end; // canMove

  // For checking death.
  if ValidIndex( Robotx, Roboty + 1 ) then
    bWasEmpty := Pixel[Robotx,Roboty + 1] = mpEmpty;

  // ============== Update map. ==============

  // flooding
  if fFlooding > 0 then
  if ( fSteps mod fFlooding ) = 0 then
  begin
    fWater := fWater + 1;
    if Assigned( OnWaterChanged ) then OnWaterChanged;
  end;

  fLambdasCount := 0;
  ops := TOperations.Create;

  if fGrowth > 0 then
  if fGrowthVar = 0 then
  begin
    for j := 1 to m do
    for i := 1 to n do
    if Pixel[i,j] = mpBeard then
    begin
      for k := j - 1 to j + 1 do
      for l := i - 1 to i + 1 do
      if ValidIndex( l,k ) and ( Pixel[ l,k ] = mpEmpty ) then
        ops.Add( l,k, mpBeard );
    end;
    fGrowthVar := fGrowth - 1;
  end else
    dec( fGrowthVar );

  for j := 1 to m do
  for i := 1 to n do
  if ( pixel[i,j] = mpLambda ) then
    AddLambda( i, j )
  else

  if ( pixel[i,j] = mpRock ) or ( pixel[i,j] = mpHighOrderRock ) then
  begin
    if ( pixel[i,j] = mpHighOrderRock ) then
       AddLambda( i, j );
    if ( ( j - 1 ) >= 1 ) and
       ( Pixel[ i, j - 1 ] = mpEmpty ) then
    begin
      if ( pixel[i,j] = mpHighOrderRock ) and
         ( not ValidIndex( i, j - 2 ) or
           ( pixel[i,j-2] <> mpEmpty ) ) then
        ops.Add( i, j - 1, mpLambda )
      else
        ops.Add( i, j - 1, pixel[i,j] );
      ops.Add( i, j, mpEmpty );
    end else

    if ( ( j - 1 ) >= 1 ) and
       ( ( Pixel[ i, j - 1 ] = mpRock ) or ( Pixel[ i, j - 1 ] = mpHighOrderRock ) ) and
       ( i + 1 <= n ) and
       ( Pixel[ i + 1, j ] = mpEmpty ) and
       ( Pixel[ i + 1, j - 1 ] = mpEmpty ) then
    begin
      if ( pixel[i,j] = mpHighOrderRock ) and
         ( not ValidIndex( i + 1, j - 2 ) or
           ( pixel[i + 1,j-2] <> mpEmpty ) ) then
        ops.Add( i+ 1, j - 1, mpLambda )
      else
        ops.Add( i + 1, j - 1, pixel[i,j] );
      ops.Add( i, j, mpEmpty );
    end else

    if ( ( j - 1 ) >= 1 ) and
       ( ( Pixel[ i, j - 1 ] = mpRock ) or ( Pixel[ i, j - 1 ] = mpHighOrderRock ) )and
       ( i - 1 >= 1 ) and
       ( Pixel[ i - 1, j ] = mpEmpty ) and
       ( Pixel[ i - 1, j - 1 ] = mpEmpty ) then
    begin
      if ( pixel[i,j] = mpHighOrderRock ) and
         ( not ValidIndex( i - 1, j - 2 ) or
           ( pixel[i - 1,j-2] <> mpEmpty ) ) then
        ops.Add( i - 1, j - 1, mpLambda )
      else
        ops.Add( i - 1, j - 1, pixel[i,j] );
      ops.Add( i, j, mpEmpty );
    end else

    if ( ( j - 1 ) >= 1 ) and
       ( Pixel[ i, j - 1 ] = mpLambda ) and
       ( i + 1 <= n ) and
       ( Pixel[ i + 1, j ] = mpEmpty ) and
       ( Pixel[ i + 1, j - 1 ] = mpEmpty ) then
    begin
      if ( pixel[i,j] = mpHighOrderRock ) and
         ( not ValidIndex( i + 1, j - 2 ) or
           ( pixel[i+1,j-2] <> mpEmpty ) ) then
        ops.Add( i+1, j - 1, mpLambda )
      else
        ops.Add( i + 1, j - 1, pixel[i,j] );
      ops.Add( i, j, mpEmpty );
    end;

  end;
  ops.Apply( Self );
  ops.Free;

  // open the Lift
  if ( fLambdaCount = 0 ) and
     ( Pixel[Liftx, Lifty] <> mpOpenedLift ) then
    setPixel( Liftx, Lifty, mpOpenedLift );

  // Check for falling rocks
  ny := Roboty + 1;
  if ny <= m then
  begin
    if ( ( Pixel[Robotx, ny] = mpRock ) or
      ( Pixel[Robotx, ny] = mpLambda ) ) and bWasEmpty then
    begin                        // was HigherRock
      fEndInfo := eiDieRock;
      Result := mrDied;
      fDone := True;
      exit;
    end;
  end;

  // Check water
  if Roboty <= fWater then
    Inc( fUnderWater )
  else
    fUnderWater := 0;

  if fWaterproof < fUnderWater then
  begin
    fEndInfo := eiDieWater;
    Result := mrDied;
    fDone := True;
    exit;
  end;

end;

function TMine.StoneCanFall( i, j: Integer ): Boolean;
begin
  result := ( ( pixel[i,j] = mpRock ) or ( pixel[i,j] = mpHighOrderRock ) ) and (
     ( ( ( j - 1 ) >= 1 ) and
       ( Pixel[ i, j - 1 ] = mpEmpty ) ) or
     ( ( ( j - 1 ) >= 1 ) and
       ( ( Pixel[ i, j - 1 ] = mpRock ) or ( Pixel[ i, j - 1 ] = mpHighOrderRock ) ) and
       ( i + 1 <= n ) and
       ( Pixel[ i + 1, j ] = mpEmpty ) and
       ( Pixel[ i + 1, j - 1 ] = mpEmpty ) ) or
     ( ( ( j - 1 ) >= 1 ) and
       ( ( Pixel[ i, j - 1 ] = mpRock ) or ( Pixel[ i, j - 1 ] = mpHighOrderRock ) )and
       ( i - 1 >= 1 ) and
       ( Pixel[ i - 1, j ] = mpEmpty ) and
       ( Pixel[ i - 1, j - 1 ] = mpEmpty ) ) or
     ( ( ( j - 1 ) >= 1 ) and
       ( Pixel[ i, j - 1 ] = mpLambda ) and
       ( i + 1 <= n ) and
       ( Pixel[ i + 1, j ] = mpEmpty ) and
       ( Pixel[ i + 1, j - 1 ] = mpEmpty ) ) );
end;

function TMine.StoneCanbeMoved( x, y: Integer ): Boolean;
var
  i,j: Integer;
begin
  // fell down;
  result := StoneCanFall( x, y );

{ // van ennel fontosabb mosdt
if not result and ( ( y - 1 >= 1 ) then
  begin

    if pixel[x,y-1] = mpLambda then
      result := true else
    if pixel[x,y-1] = mpRobot then
      result := true else
    if ( pixel[x,y-1] = mpBeard ) and
      ( Razor > 0 ) then
      result := true else

    if pixel[x,y-1] = mpRock highorder is then
      result := StoneCanBeMoved( x. y - 1 );

    if not result and ( x + 1 <= n ) then
    begin

    end else
    if x - 1 >= 1 then
    begin

    end;

}
end;


function TMine.AreThereMovingStones: Boolean;
var
  i,j: Integer;

begin
  j := 1;
  result := false;
  while ( j <= m ) and not result do
  begin
    i := 1;
    while ( i <= n ) and not result do
    begin
      result := StoneCanFall( i,j );
      inc( i );
    end;
    inc( j );
  end;

end;

procedure TMine.AddLambda( x, y: Integer );
begin
  fLambdas[fLambdasCount].X := x;
  fLambdas[fLambdasCount].Y := y;
  inc( fLambdasCount );
end;

function TMine.getLambda( indx: Integer ): TPoint;
begin
  result := fLambdas[indx];
end;


procedure TMine.setLambda(indx: Integer; const Value: TPoint);
begin
  fLambdas[indx] := value;
end;

function TMine.UnderWaterLambdaCount: Integer;
var
  i: Integer;
begin
  result := 0;
  for i := 0 to LambdaCount - 1 do
  if Lambda[i].y <= fWater then
    inc( Result );
end;


function TMine.PossibleGoThere( x, y: Integer ): Boolean;

var
  dx,dy: Integer;

  lastcross,
  newcross: TPoint;
  cangoL,
  cangoR,
  cangoU,
  cangoD: Boolean;


begin
  GoFill( Robotx, Roboty );

  dx := Sign( x - Robotx );
  dy := Sign( y - Roboty );

  lastcross.x := Robotx;
  lastcross.y := Roboty;
  cangoL := cangoThere( lastcross.x - 1, lastcross.y);
  cangoR := cangoThere( lastcross.x + 1, lastcross.y);
  cangoU := cangoThere( lastcross.x, lastcross.y + 1);
  cangoD := cangoThere( lastcross.x, lastcross.y - 1);

  while cangoL or cangoR or cangoU or cangoD do
  begin
    if dx = -1 then
    begin

      if lastcross.X
    end;

  end;

end;

end.
