unit umineContainer;

{$I fpc.inc}

interface

uses
    Classes
  , SysUtils
  ;

type
  EMineException = class( Exception )
  end;

  PMinePixel = ^TMinePixel;
  TMinePixel = (
    mpRobot,
    mpWall,
    mpRock,
    mpLambda,
    mpClosedLift,
    mpOpenedLift,
    mpEarth,
    mpEmpty,

    mpTrampolinA,
    mpTrampolinB,
    mpTrampolinC,
    mpTrampolinD,
    mpTrampolinE,
    mpTrampolinF,
    mpTrampolinG,
    mpTrampolinH,
    mpTrampolinI,

    mpTarget1,
    mpTarget2,
    mpTarget3,
    mpTarget4,
    mpTarget5,
    mpTarget6,
    mpTarget7,
    mpTarget8,
    mpTarget9,

    mpBeard,
    mpRazor,

    mpHighOrderRock
  );

  TMovement = (
    mLeft,
    mRight,
    mUp,
    mDown,
    mWait,
    mAbort,
    mShave
  );

const
  cMinePixelStr: Array[TMinePixel] of Char = (
    'R',
    '#',
    '*',
    '\',
    'L',
    'O',
    '.',
    ' ',

    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',

    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',

    'W',
    '!',

    '@'

  );

  cMovements: Array[TMovement] of Char = (
    'L',
    'R',
    'U',
    'D',
    'W',
    'A',
    'S'
  );

  cDefPixelArrayInc = 1000;
type

  TMovementIntA = Array[TMovement] of Integer;

  TMinePixelList = class
    fPixels: Array of TMinePixel;
    fCapacity: Integer;
    fCount: Integer;
    fInc: Integer;
  private


    function getPixel(i: Integer): TMinePixel;
  public
    constructor Create( lInc: Integer = cDefPixelArrayInc );
    destructor Destroy; override;

    procedure Add( pxl: TMinePixel );

    property Pixel[i: Integer]: TMinePixel read getPixel;

    property Count: Integer read fCount;
  end;

  TPixelChanging = procedure( x, y: Integer; pfrom, pto: TMinePixel ) of object;

  TMineContainer = class
  private
    fMinePixels: Array of Array of TMinePixel;
    fColor: Array of Array of Byte;
    fFillColor: Array[0..1] of Byte;
    fn,fm: Integer;

    // events.
    fOnPixelChanging: TPixelChanging;
    procedure setColor(i, j: Integer; const Value: Byte);
    function getColor(i, j: Integer): Byte;

    function getPixel(i, j: Integer): TMinePixel;


  protected


  public
    constructor Create( tsl: TStrings ); overload;
    procedure setPixel( i, j: Integer; const Value: TMinePixel );

    function ValidIndex( i, j: Integer ): Boolean;
    function CharToMinePixl( lChar: Char ): TMinePixel;

    property pixel[i,j: Integer]: TMinePixel read getPixel;
    property n: Integer read fn;
    property m: Integer read fm;

    procedure GoFill( x,y : integer );

    property Color[i,j: Integer]: Byte read getColor write setColor;

    property OnPixelChanging: TPixelChanging read fOnPixelChanging write fOnPixelChanging;
  end;

function Str2Movement( str: String ): TMovement;
implementation

uses

    Math
  ;

function Str2Movement( str: String ): TMovement;
begin
  result := Low( TMovement );
  while ( result <= High( TMovement ) ) and
        ( cMovements[result] <> str ) do
    Inc( result );
  if result > High( TMovement ) then
    result := mWait
//    raise EMineException.Create( 'Unknown command!' );
end;

{ TMinePixelList }

constructor TMinePixelList.Create( lInc: Integer = cDefPixelArrayInc );
begin
  fCapacity := 0;
  fCount := 0;
  fInc := lInc;
end;

destructor TMinePixelList.Destroy;
begin
  fPixels := nil;
  inherited;
end;

function TMinePixelList.getPixel( i: Integer ): TMinePixel;
begin
  {$ifdef debug}
  if ( i < 1 ) or ( i > fCount ) then
    raise EMineException.Create( '[TMinePixelList.getPixel]: Wrong indx: ' + IntToStr( i ) );
  {$endif}
  result := fPixels[i];
end;

procedure TMinePixelList.Add( pxl: TMinePixel );
begin
  if fCount = fCapacity then
  begin
    fCapacity := fCapacity + fInc;
    SetLength( fPixels, fCapacity );
  end;
  fPixels[fCount] := pxl;
  Inc( fCount );
end;

{ TMineContainer }

/// Input: Just the pure raw map, without metadata.
constructor TMineContainer.Create( tsl: TStrings );
var
  i,j: Integer;
  s: String;
begin
  fn := 0;
  fm := 0;
  for i := 0 to tsl.Count - 1 do
    fn := Max( fn, Length( tsl[i] ) );
  fm := tsl.Count;

  SetLength( fMinePixels, n, m );
  SetLength( fColor, n + 2, m + 2 );
  for j := 0 to m + 1 do
  for i := 0 to n + 1 do
    fColor[i,j] := $40;

  for j := 0 to m - 1 do
  for i := 0 to n - 1 do
  if Length( tsl[j] ) < i+1 then
    setPixel(i + 1, m - j, mpEmpty )
//    fMinePixels[i,m - j - 1 ] := mpEmpty
  else
    setPixel(i + 1, m - j, CharToMinePixl( tsl[j][i+1] ) );
//    fMinePixels[i,m - j - 1 ] := CharToMinePixl( tsl[j][i+1] );
end;

function TMineContainer.CharToMinePixl( lChar: Char ): TMinePixel;
begin
  result := Low( TMinePixel );
  while ( result <= High( TMinePixel ) ) and
        ( cMinePixelStr[result] <> lChar ) do
    inc( result );
  if result > High( TMinePixel ) then
    raise EMineException.Create( '[TMineContainer.CharToMinePixl]: Unknown pixel: ' + lChar );
end;

function TMineContainer.ValidIndex(i, j: Integer): Boolean;
begin
  result := ( i >= 1 ) and ( i <= n ) and
     ( j >= 1 ) and ( j <= m );
end;

function TMineContainer.getPixel( i, j: Integer ): TMinePixel;
begin
  {$ifdef debug}
  if not ValidIndex( i, j ) then
    raise EMineException.Create( '[TMineContainer.getPixel]: Invalid index: ( ' + inttoStr( i ) + ', ' + IntToStr( j ) + ')' );
  {$endif}
  result := fMinePixels[i-1,j-1];
end;

// Do not call directly if possible, just through the TOperationList class
procedure TMineContainer.setPixel( i, j: Integer; const Value: TMinePixel );
begin
  {$ifdef debug}
  if not ValidIndex( i, j ) then
    raise EMineException.Create( '[TMineContainer.getPixel]: Invalid index: ( ' + inttoStr( i ) + ', ' + IntToStr( j ) + ')' );
  {$endif}
  if Assigned( fOnPixelChanging ) then
    fOnPixelChanging( i, j, fMinePixels[i-1,j-1], Value );

  fMinePixels[i-1,j-1] := Value;

  if ( pixel[i,j] = mpLambda ) or
    ( pixel[i,j] = mpRobot ) or
    ( pixel[i,j] = mpOpenedLift ) or
    ( pixel[i,j] = mpEarth ) or
    ( pixel[i,j] = mpEmpty ) or
    ( ( pixel[i,j] >= mpTrampolinA ) and
      ( pixel[i,j] <= mpTrampolinI ) ) or
    ( pixel[i,j] = mpRazor ) then
    fColor[i,j] := 0
  else
    fColor[i,j] := $40;

end;

function TMineContainer.getColor( i, j: Integer ): Byte;
begin
  {$ifdef debug}
  if not ValidIndex( i, j ) then
    raise EMineException.Create( '[TMineContainer.getColor]: Invalid index: ( ' + inttoStr( i ) + ', ' + IntToStr( j ) + ')' );
  {$endif}
  result := fColor[i,j];
end;

procedure TMineContainer.setColor(i, j: Integer; const Value: Byte);
begin

end;


{
 bytearray 0,1: entrydirection
             3: startposition
             6: borderdetect
             7: fill
}

procedure TMineContainer.GoFill( x,y : integer );

var dir,b : byte;
    x1,y1 : integer;
label free,fill,nextpixel,previouspixel;

begin

 try

//----------startup
  for y1 := 1 to m  do
  for x1 := 1 to n  do
  if fColor[x1,yy] <> $40 then
    fColor[x1,yy] := $0;


  fColor[x,y] := $88;          //set startdir,fillbit
  dir := 0;

nextpixel:

  case dir of
   0 : inc(x);
   1 : dec(x);
   2 : dec(y);
   3 : inc(y);
  end;
  if (fColor[x,y] and $c0) <> 0 then goto previouspixel;
  fColor[x,y] := $80 or dir;//record fill + entry-direction
  if dir <> 1 then dir := 0;//compute exit-direction
  goto nextpixel;

previouspixel:

 case dir of
  0 : dec(x);
  1 : inc(x);
  2 : inc(y);
  3 : dec(y);
 end;
 b := fColor[x,y];
 inc(dir);
 if (b and $f) = (dir xor 1) then inc(dir);//skip entry-direction
 if dir > 3 then
  begin
   dir := b and $f;
   if dir >= 8 then goto fill else goto previouspixel;
  end
 else goto nextpixel;

fill:
free:
 finally
 end;
end;

end.
