unit uengine;

{$I fpc.inc}

interface

uses
    Classes
  {$ifdef linux}
  , baseunix
  {$else}
  //, Libc
  {$endif}
  , umineContainer
  , umine
  , ugraph
  ;

type
  // ha aweight 0 akkor azzal megakadalyozza hogy tovabbmenjen.
  TConditionProc  = procedure( node: PNode; cmd: TMovement ; var weight: Integer ) of object;



  TEngine = class
  private
    fSigInt: Boolean;
    fSigIntTime: TDateTime;
    fGraph: TGraph;
    fMaxScore: Integer;
    fCommand: String;

    fMaxSolveStep: Integer;
    function Find( procs: Array of TConditionProc ): String;

    function MovementIntAStr( inp: TMovementIntA ): String;

  protected

    procedure SkipDone( node: PNode; cmd: TMovement; var weight: Integer );
    procedure ValidMovement( node: PNode; cmd: TMovement; var weight: Integer );
    procedure NoRepeatPath( node: PNode; cmd: TMovement; var weight: Integer );
    procedure LessWait( node: PNode; cmd: TMovement; var weight: Integer );
    procedure AbortControl( node: PNode; cmd: TMovement; var weight: Integer );
    procedure LiftWatcher( node: PNode; cmd: TMovement; var weight: Integer );
    procedure SolveStepWatcher( node: PNode; cmd: TMovement; var weight: Integer );
    procedure NotWorth( node: PNode; cmd: TMovement; var weight: Integer );

    procedure LambdaHunter( node: PNode; cmd: TMovement; var weight: Integer );
    // for flooding.
    procedure FloodingCheck( node: PNode; cmd: TMovement; var weight: Integer );
    procedure PossibleCheck( node: PNode; cmd: TMovement; var weight: Integer );

  public
    function FindBruce: String;

    procedure CheckResult;
    constructor Create( lgraph: TGraph );
    destructor destroy; override;

    property Cmd: String read fCommand;
    property Score: Integer read fMaxScore;
  end;

implementation

uses
    SysUtils
  , dateutils
  , Math
  {$ifndef linux}
  , windows
  {$endif}
  ;

type
  TIntegerA = Array of Integer;

var
  leng: TEngine;

{$ifdef linux}
procedure sig_int( sig: Longint ); cdecl;
begin
  leng.fSigInt := True;
  leng.fSigIntTime := Now;
  //writeln( 'sigint: ' + DateTimeToStr(Now) );
end;

{$else}
function console_handler( dwCtrlType: DWORD ): BOOL; stdcall;
begin
  // Avoid terminating with Ctrl+C
  if (  CTRL_C_EVENT = dwCtrlType  ) then
  begin
    leng.fSigInt := True;
    leng.fSigIntTime := Now;
    result := TRUE
  end else
    result := FALSE;
end;
{$endif}

{ TEngine }

constructor TEngine.Create(lgraph: TGraph);
begin
  leng := self;
  fSigInt := false;
  {$ifdef linux}
  fpsignal( SIGINT, signalhandler(@sig_int));
  {$else}
  Windows.setConsoleCtrlHandler( @console_handler, True );
  {$endif}
  fGraph := lGraph;
end;

destructor TEngine.destroy;
begin
  // code...
  inherited;
end;

function TEngine.MovementIntAStr( inp: TMovementIntA ): String;
var
  m: TMovement;
begin
  result := '';
  for m := Low( TMovement ) to High( TMovement ) do
    result := result + cMovements[m] + ':' + IntToStr( inp[m] ) + ' ';
end;

procedure TEngine.CheckResult;
begin
  if fGraph.Mine.Done then
  begin
    if fGraph.Mine.Score > fMaxScore then
    begin
      fMaxScore := fGraph.Mine.Score;
      fCommand := fGraph.GetPath;
      //form1.mLog.Lines.Add('New record: ' + IntToStr( fMaxScore ) + ', Cmd: ' + fCommand );
      //Writeln('New record: ' + IntToStr( fMaxScore ) + ', Cmd: ' + fCommand );
    end;
  end;
end;

function TEngine.Find(procs: array of TConditionProc): String;
var
  i,j,w: Integer;
  m,mmax: TMovement;
  possib: TMovementIntA;
  mt: TMoveResult;
begin
  w := 0;
  try

    for m := Low( TMovement ) to High( TMovement ) do
    begin
       //A nodebol nincs tovabb.
      if ( fGraph.State^.children[m] <> nil ) and
        ( fGraph.State^.children[m]^.nto = nil ) then
        possib[m] := 0

      else begin
        possib[m] := 100;
        i := 0;
        while ( i < Length( procs ) ) do
        begin

          procs[i]( fgraph.State, m, w );
          if w = -MaxInt then
          begin
            // do not do this
            possib[m] := -MaxInt;
            break;
          end;

          possib[m] := possib[m] + w;
          inc( i );
        end;
      end;
    end;


    repeat
      i := 0;
      j := 0;
      for m := Low( TMovement ) to High( TMovement ) do
      begin
        if possib[m] > i then
        begin
          i := possib[m];
          mmax := m;
          inc( j );
        end;
      end;

      if ( j > 0 ) and ( possib[mmax] > 50 )  then
      begin
        //Form1.mLog.Lines.Add( fGraph.GetPath + '; ' + MovementIntAStr( possib ) );

        possib[mmax] := 0;
        mt := fGraph.DoMove( mmax );

        CheckResult;
        if fGraph.State^.done then
        begin
          if mt = mrSuccess then
            fMaxSolveStep := fGraph.Mine.Steps;
        end else
          Find( procs ); // Recursive

        fGraph.Undo;
      end else j := 0;
    until ( j = 0 ) or ( fSigInt and ( MilliSecondsBetween( Now, fSigIntTime ) > 9000 ) );
  except
    result := 'A'; // Last resort.
  end;
end;

// ------------------ Bruce -------------------
function TEngine.FindBruce: String;
begin
  fMaxScore := 0;
  fMaxSolveStep := fGraph.Mine.n * fGraph.Mine.m;

  Result := Find( [
    {$ifdef fpc}@{$endif}SkipDone,
    {$ifdef fpc}@{$endif}LessWait,
    {$ifdef fpc}@{$endif}AbortControl,
    {$ifdef fpc}@{$endif}NotWorth,
    {$ifdef fpc}@{$endif}FloodingCheck,
    {$ifdef fpc}@{$endif}SolveStepWatcher,
    {$ifdef fpc}@{$endif}LiftWatcher,
    {$ifdef fpc}@{$endif}ValidMovement,
    {$ifdef fpc}@{$endif}NoRepeatPath,
    {$ifdef fpc}@{$endif}LambdaHunter,
    {$ifdef fpc}@{$endif}PossibleCheck
  ]);
  if fSigInt then
  begin
    //writeln(' -> [Ctrl+C] ' );
  end;

end;

procedure TEngine.SkipDone( node: PNode; cmd: TMovement; var weight: Integer );
begin
  if node^.done then
    weight := -MaxInt
  else
    weight := 1;
end;

procedure TEngine.ValidMovement( node: PNode; cmd: TMovement;
  var weight: Integer);
begin
  if fGraph.Mine.canMove( cmd ) then
    weight := 1
  else
    weight := -MaxInt;
end;

procedure TEngine.NoRepeatPath(node: PNode; cmd: TMovement; var weight: Integer);
var
  pn: PNode;
  dx, dy,
  rpx,rpy: Integer;
  wasthere: Integer;
begin
  dx := 0;
  dy := 0;
  wasThere := 0;
  case cmd of
    mLeft: dx := -1;
    mRight: dx := 1;
    mUp: dy := 1;
    mDown: dy := -1;
  end;
  if ( dx <> 0 ) or ( dy <> 0 ) then
  begin
    pn := node;
    while pn^.parent <> nil do
    begin
      pn^.parent^.op.RobotPos( rpx, rpy );
      if ( rpx = fGraph.Mine.Robotx + dx ) and ( rpy = fGraph.Mine.Roboty + dy ) then
        inc( wasThere );
      pn := pn^.parent^.nfrom;
    end;

    if wasthere = 0 then
      weight := 10
    else begin
      if wasthere > 3 then
        weight := -MaxInt
      else
        weight := -50 * wasthere;

    end
  end else
    weight := 1;
end;

procedure TEngine.LessWait( node: PNode; cmd: TMovement; var weight: Integer );
begin
  if cmd = mWait then
  begin
    if fGraph.Mine.AreThereMovingStones then
      weight := 5
    else
      weight := -MaxInt;
  end else
    weight := 1;
end;

procedure TEngine.AbortControl( node: PNode; cmd: TMovement; var weight: Integer );
var
  lScore: Integer;
begin
  if cmd = mAbort then
  begin
    weight := -MaxInt;
    if ( node^.parent <> nil ) and
       ( node^.parent^.nfrom^.fLambdaCount > fGraph.Mine.LambdaCount )
       then
    begin
      lScore := fGraph.Mine.Score + ( fGraph.Mine.InitialLambdaCount -
        fGraph.Mine.LambdaCount ) * 25;
      if lScore > fMaxScore then
      begin
        fMaxScore := lScore;
        fCommand := fGraph.GetPath + 'A';
      end;
    end;
  end else
    weight := 1;
end;

procedure TEngine.LiftWatcher( node: PNode; cmd: TMovement; var weight: Integer );
var
  x,y,
  dx,dy: Integer;
  bUp,bDown,bLeft,bRight: Boolean;
  mp: TMinePixel;
begin
  // check is it reachable.
  x := fGraph.Mine.Liftx;
  y := fGraph.Mine.Lifty;

  fGraph.Mine.GoFill( fGraph.Mine.Robotx, fGraph.Mine.Roboty );
  if fGraph.Mine.Color[x,y] = $0 then
  begin
    weight := -MaxInt;
    exit;
  end;

{  bUp := fGraph.Mine.ValidIndex( x, y + 1 );
  if bUp then
  begin // narrow
    mp := fGraph.Mine.pixel[x, y + 1];

    bUp := ( mp <> mpWall ) and ( ( mp <>  mpRock ) or ( mp <>  mpHighOrderRock ) );
    if not bUp then
    begin
      if mp = mpBeard then
        bUp := fGraph.Mine.Razors > 0
      else
        bUp := false;
    end;
  end;

  bDown := fGraph.Mine.ValidIndex( x, y - 1 );
  if bDown then
  begin // narrow
    mp := fGraph.Mine.pixel[x, y -1 ];
    bDown := ( mp <> mpWall ) and ( ( mp <>  mpRock ) or ( mp <>  mpHighOrderRock ) );
    if bDown then
    begin

    end;
  end;

  bLeft := fGraph.Mine.ValidIndex( x -  1, y );
  if bLeft then
  begin // narrow
    mp := fGraph.Mine.pixel[x - 1, y ];
    bLeft := ( mp <> mpWall ) and ( ( mp <>  mpRock ) or ( mp <>  mpHighOrderRock ) );
    if bLeft then
    begin

    end;
  end;

  bRight := fGraph.Mine.ValidIndex( x +  1, y );
  if bRight then
  begin // narrow
    mp := fGraph.Mine.pixel[x + 1, y ];
    bRight := ( mp <> mpWall ) and ( ( mp <>  mpRock ) or ( mp <>  mpHighOrderRock ) );
    if bRight then
    begin

    end;
  end;

  if not bUp and not bDown and not bRight and not BLeft then
//    weight := -MaxInt
    weight := 1
  else}
  begin
    if fGraph.Mine.pixel[x, y ] = mpOpenedLift then
    begin
      if cmd = mWait then
        weight := 100
      else begin
        dx := 0;
        dy := 0;
        case cmd of
          mLeft: dx := -1;
          mRight: dx := 1;
          mUp: dy := 1;
          mDown: dy := -1;
        end;

        if ( Sign( x - fGraph.Mine.Robotx ) <= dx ) or
           ( Sign( y - fGraph.Mine.Roboty ) <= dy ) then
          weight := 1000
        else
          weight := 500;

      end;
    end else
      weight := 1;
  end;
end;

procedure TEngine.SolveStepWatcher( node: PNode; cmd: TMovement; var weight: Integer );
begin
  if fGraph.Mine.Steps < fMaxSolveStep then
    weight := 1
  else
    weight := -MaxInt;
end;

procedure TEngine.NotWorth( node: PNode; cmd: TMovement; var weight: Integer );
var
  pp: Integer;
  i,imax, l, lmax: Integer;
begin
  pp := fGraph.Mine.Score +
    fGraph.Mine.LambdaCount * 25 +
    fGraph.Mine.InitialLambdaCount * 50;


  if fGraph.Mine.LambdaCount > 0 then
  begin
    imax := 0;
    lmax := 0;
    for i := 0 to fGraph.Mine.LambdaCount - 1 do
    begin
      l :=
        Abs( fGraph.Mine.Lambda[i].x - fGraph.Mine.Robotx ) +
        Abs( fGraph.Mine.Lambda[i].y - fGraph.Mine.Roboty ) +
        Abs( fGraph.Mine.Lambda[i].x - fGraph.Mine.Liftx ) +
        Abs( fGraph.Mine.Lambda[i].y - fGraph.Mine.Lifty )
        ;
      if l > lmax then
      begin
        lmax := l;
        imax := i;
      end;
    end;

    // elmenni a legtavolabbi lambdaert es a lifthez
    pp := pp - lmax;

  end else
  begin
    pp := pp -
      Abs( fGraph.Mine.Liftx - fGraph.Mine.Robotx ) -
      Abs( fGraph.Mine.Lifty - fGraph.Mine.Roboty );

  end;

  if pp <= fMaxScore then
    weight := -MaxInt
  else
    weight := 1;

end;

procedure TEngine.LambdaHunter( node: PNode; cmd: TMovement; var weight: Integer );

procedure calc( lx, ly, dx, dy: Integer );
const
  radi = 10;
var
  ii,jj, ll: Integer;
  ltox,ltoy: Integer;
begin
  if lx = 1 then
    ltox :=Min( fGraph.Mine.n, fGraph.Mine.Robotx + radi )
  else
    ltox :=Max( 1, fGraph.Mine.Robotx - radi );

  if ly = 1 then
    ltoy :=Min( fGraph.Mine.m, fGraph.Mine.Roboty + radi )
  else
    ltoy :=Max( 1, fGraph.Mine.Roboty - radi );


  jj := fGraph.Mine.Roboty + ly + dy;
  while jj <> ltoy do
  begin
    ii := fGraph.Mine.Robotx + lx + dx;
    while ( ii <> ltox ) do
    begin
      if fGraph.Mine.pixel[ii,jj] = mpLambda then
      begin
        ll := abs( fGraph.Mine.Roboty  - jj ) +
          abs( fGraph.Mine.Robotx - ii );
        weight := weight + sqr( radi - ll ) * sqr( radi - ll )
      end;

      ii := ii + lx
    end;
    jj := jj + ly;
  end;
end;

begin
  weight := 1;

  case cmd of
    mLeft:
    begin
      calc( -1, 1, 0, -1 );
      calc( -1, -1, 0, 0 );
    end;

    mRight:
    begin
      calc( 1, 1, 0, -1 );
      calc( 1, -1, 0, 0 );
    end;

    mUp:
    begin
      calc( 1, 1, -1, 0 );
      calc( -1, 1, 0, 0 );
    end;

    mDown:
    begin
      calc( 1,-1, -1, 0 );
      calc( -1, -1, 0, 0 );
    end;
    else exit;
  end;
end;

procedure TEngine.FloodingCheck( node: PNode; cmd: TMovement; var weight: Integer );
var
  i: Integer;
begin
  if fGraph.Mine.Water > 0 then
  begin
    if ( fGraph.Mine.Lifty <= fGraph.Mine.Water ) then

//    if fGraph.Mine.Water = fGraph.Mine.m - 1 then
    begin
      // a -1 a falak miatt van.
      if ( Abs( fGraph.Mine.Roboty - fGraph.Mine.Lifty ) +
           Abs( fGraph.Mine.Robotx - fGraph.Mine.Liftx ) ) >
           ( fGraph.Mine.Waterproof - fGraph.Mine.UnderWater ) then
        weight := -MaxInt;
    end;

    i := fGraph.Mine.UnderWaterLambdaCount;
    if i = 0 then
      weight := 1
    else
    begin
      if fGraph.Mine.Roboty > fGraph.Mine.Water then
        i := i + Abs( fGraph.Mine.Roboty - fGraph.Mine.Water );

      if i > fGraph.Mine.Waterproof then
        weight := -MaxInt
      else begin
        if cmd = mDown then
          weight := 100 else
        if cmd = mAbort then
          weight := -50 else
        weight := 1;
      end;
    end;
  end else
    weight := 1;
end;

procedure TEngine.PossibleCheck( node: PNode; cmd: TMovement; var weight: Integer );
var
  i: Integer;
  b: Boolean;

begin
  fGraph.Mine.GoFill( fGraph.Mine.Robotx, fGraph.Mine.Roboty );
  b := True;
  i := 0;
  while ( i < fGraph.Mine.LambdaCount ) and b do
  begin
    b := fGraph.Mine.Color[ fGraph.Mine.Lambda[i].x, fGraph.Mine.Lambda[i].y ] <> $0;
    inc( i );
  end;

  if not b then
    weight := -MaxInt
  else
    weight := 1;
end;

end.
