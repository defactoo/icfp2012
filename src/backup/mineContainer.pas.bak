unit mineContainer;

interface

uses
    Classes
  , SysUtils
  ;

type
  EMineException = class( Exception )
  end;

  PMinePixel = ^TMinePixel;
  TMinePixel = (
    mpRobot,
    mpWall,
    mpRock,
    mpLambda,
    mpClosedLift,
    mpOpenedLift,
    mpEarth,
    mpEmpty,

    mpTrampolinA,
    mpTrampolinB,
    mpTrampolinC,
    mpTrampolinD,
    mpTrampolinE,
    mpTrampolinF,
    mpTrampolinG,
    mpTrampolinH,
    mpTrampolinI,

    mpTarget1,
    mpTarget2,
    mpTarget3,
    mpTarget4,
    mpTarget5,
    mpTarget6,
    mpTarget7,
    mpTarget8,
    mpTarget9,

    mpBeard,
    mpRazor
  );

  TMovement = (
    mLeft,
    mRight,
    mUp,
    mDown,
    mWait,
    mAbort,
    mShave
  );

const
  cMinePixelStr: Array[TMinePixel] of Char = (
    'R',
    '#',
    '*',
    '\',
    'L',
    'O',
    '.',
    ' ',

    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',

    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',

    'W',
    '!'

  );

  cMovements: Array[TMovement] of Char = (
    'L',
    'R',
    'U',
    'D',
    'W',
    'A',
    'S'
  );

  cDefPixelArrayInc = 1000;
type

  TMovementIntA = Array[TMovement] of Integer;
  
  TMinePixelList = class
    fPixels: Array of TMinePixel;
    fCapacity: Integer;
    fCount: Integer;
    fInc: Integer;
  private


    function getPixel(i: Integer): TMinePixel;
  public
    constructor Create( lInc: Integer = cDefPixelArrayInc );
    destructor Destroy; override;

    procedure Add( pxl: TMinePixel );

    property Pixel[i: Integer]: TMinePixel read getPixel;

    property Count: Integer read fCount;
  end;

  TPixelChanging = procedure( x, y: Integer; pfrom, pto: TMinePixel ) of object;

  TMineContainer = class
  private
    fMinePixels: Array of Array of TMinePixel;
    fn,fm: Integer;

    // events.
    fOnPixelChanging: TPixelChanging;

    function getPixel(i, j: Integer): TMinePixel;

    function CharToMinePixl( lChar: Char ): TMinePixel;
  protected


  public
    constructor Create( tsl: TStrings ); overload;
    procedure setPixel( i, j: Integer; const Value: TMinePixel );

    function ValidIndex( i, j: Integer ): Boolean;
        
    property pixel[i,j: Integer]: TMinePixel read getPixel;
    property n: Integer read fn;
    property m: Integer read fm;

    property OnPixelChanging: TPixelChanging read fOnPixelChanging write fOnPixelChanging;
  end;

function Str2Movement( str: String ): TMovement;
implementation

uses

    Math
  ;

function Str2Movement( str: String ): TMovement;
begin
  result := Low( TMovement );
  while ( result <= High( TMovement ) ) and
        ( cMovements[result] <> str ) do
    Inc( result );
  if result > High( TMovement ) then
    result := mWait
//    raise EMineException.Create( 'Unknown command!' );
end;

{ TMinePixelList }

constructor TMinePixelList.Create( lInc: Integer = cDefPixelArrayInc );
begin
  fCapacity := 0;
  fCount := 0;
  fInc := lInc;
end;

destructor TMinePixelList.Destroy;
begin
  fPixels := nil;
  inherited;
end;

function TMinePixelList.getPixel( i: Integer ): TMinePixel;
begin
  {$ifdef debug}
  if ( i < 1 ) or ( i > fCount ) then
    raise EMineException.Create( '[TMinePixelList.getPixel]: Wrong indx: ' + IntToStr( i ) );
  {$endif}
  result := fPixels[i];
end;

procedure TMinePixelList.Add( pxl: TMinePixel );
begin
  if fCount = fCapacity then
  begin
    fCapacity := fCapacity + fInc;
    SetLength( fPixels, fCapacity );
  end;
  fPixels[fCount] := pxl;
  Inc( fCount );
end;

{ TMineContainer }

/// Input: Just the pure raw map, without metadata.
constructor TMineContainer.Create( tsl: TStrings );
var
  i,j: Integer;
  s: String;
begin
  fn := 0;
  fm := 0;
  for i := 0 to tsl.Count - 1 do
    fn := Max( fn, Length( tsl[i] ) );
  fm := tsl.Count;

  SetLength( fMinePixels, n, m );

  for j := 0 to m - 1 do
  for i := 0 to n - 1 do
  if Length( tsl[j] ) < i+1 then
    fMinePixels[i,m - j - 1 ] := mpEmpty
  else
    fMinePixels[i,m - j - 1 ] := CharToMinePixl( tsl[j][i+1] );
end;

function TMineContainer.CharToMinePixl( lChar: Char ): TMinePixel;
begin
  result := Low( TMinePixel );
  while ( result <= High( TMinePixel ) ) and
        ( cMinePixelStr[result] <> lChar ) do
    inc( result );
  if result > High( TMinePixel ) then
    raise EMineException.Create( '[TMineContainer.CharToMinePixl]: Unknown pixel: ' + lChar );
end;

function TMineContainer.ValidIndex(i, j: Integer): Boolean;
begin
  result := ( i >= 1 ) and ( i <= n ) and
     ( j >= 1 ) and ( j <= m );
end;

function TMineContainer.getPixel( i, j: Integer ): TMinePixel;
begin
  {$ifdef debug}
  if not ValidIndex( i, j ) then
    raise EMineException.Create( '[TMineContainer.getPixel]: Invalid index: ( ' + inttoStr( i ) + ', ' + IntToStr( j ) + ')' );
  {$endif}
  result := fMinePixels[i-1,j-1];
end;

// Do not call directly if possible, just through the TOperationList class
procedure TMineContainer.setPixel( i, j: Integer; const Value: TMinePixel );
begin
  {$ifdef debug}
  if not ValidIndex( i, j ) then
    raise EMineException.Create( '[TMineContainer.getPixel]: Invalid index: ( ' + inttoStr( i ) + ', ' + IntToStr( j ) + ')' );
  {$endif}
  if Assigned( fOnPixelChanging ) then
    fOnPixelChanging( i, j, fMinePixels[i-1,j-1], Value );
  fMinePixels[i-1,j-1] := Value;
end;

end.
