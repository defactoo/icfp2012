unit uhelper;

interface

uses
    Classes
  , uMineContainer
  ;

const
  cDefpRecInc = 10000;

type
  TpRec = record
    x,y: Integer;
  end;

  TpRecList = class
  private
    list: Array of TpRec;
  protected
    fCount,
    fCapacity,
    fDefInc: Integer;
  public
    constructor Create( lDefInc: Integer = cDefpRecInc );
    destructor Destroy; override;
    procedure Clear;

    procedure Add( x,y: Integer );
    function Exists( x,y: Integer ): Boolean;
  end;

implementation

{ TpRecList }

procedure TpRecList.Add( x, y: Integer );
begin
  if Exists( x, y ) then
    exit;
  if fCapacity = fCount then
  begin
    Inc( fCapacity, fDefInc );
    SetLength( list, fCapacity );
  end;

  list[fCount].x := x;
  list[fCount].y := y;
  inc( fCount );
end;

procedure TpRecList.Clear;
begin
  fCount := 0;
end;

constructor TpRecList.Create(lDefInc: Integer);
begin
  fDefInc := lDefInc;
  fCount := 0;
  fCapacity := 0;
end;

destructor TpRecList.Destroy;
begin
  list := nil;
  inherited;
end;

function TpRecList.Exists(x, y: Integer): Boolean;
var
  i: Integer;
begin
  i := 0;
  while ( i < fCount ) and not ( ( list[i].x = x ) and ( list[i].y = y ) ) do
    inc( i );
  result := i < fCount;
end;

end.
