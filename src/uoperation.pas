unit uoperation;

{$I fpc.inc}

interface

uses
    Classes
  , SysUtils
  , umineContainer
  ;

const
  cDefOpArrayInc = 100;
type
  TOperation = record
    x,
    y: Integer;
    vFrom,
    vTo: TMinePixel;
  end;

  TOperationA = Array of TOperation;

  TOperations = class
  private
    fOperations: TOperationA;
    fCapacity: Integer;
    fCount: Integer;
    fInc: Integer;
    function getOp(indx: Integer): TOperation;
    function IndexOf( x, y: Integer ): Integer;
  protected
  public
    constructor Create( lInc: Integer = cDefOpArrayInc );
    destructor Destroy; override;

    // This is for the internal use,
    procedure Add( x, y: Integer; Value: TMinePixel; bCheck: Boolean = false );
    // this is for the graph
    procedure Add2( x, y: Integer; ValueFrom, ValueTo: TMinePixel );

    procedure RobotPos( var x,y: Integer );
    procedure Apply( lmine: TMineContainer );
    procedure Revert( lmine: TMineContainer );

    property Count: Integer read fCount;
    property Op[indx: Integer]: TOperation read getOp;
  end;

implementation

type
  EOperationException = class( Exception )
  end;

procedure QuickSort( var A: TOperationA; iLo, iHi: Integer ) ;

 function val( tp: TOperation ): integer;
   begin
     result := tp.y * 1000000 + tp.x;
   end;

 var
   Lo, Hi: Integer;
   T, Pivot: TOperation;
 begin
   Lo := iLo;
   Hi := iHi;
   Pivot := A[(Lo + Hi) div 2];
   repeat
     while ( val( A[Lo] ) < val( Pivot ) ) do Inc(Lo) ;
     while ( val( A[Hi] ) > val( Pivot ) ) do Dec(Hi) ;
     if Lo <= Hi then
     begin
       T := A[Lo];
       A[Lo] := A[Hi];
       A[Hi] := T;
       Inc(Lo) ;
       Dec(Hi) ;
     end;
   until Lo > Hi;
   if Hi > iLo then QuickSort(A, iLo, Hi) ;
   if Lo < iHi then QuickSort(A, Lo, iHi) ;
 end;

{ TOperationList }

constructor TOperations.Create( lInc: Integer = cDefOpArrayInc );
begin
  fCapacity := 0;
  fCount := 0;
  fInc := lInc;
end;

destructor TOperations.Destroy;
begin
  fOperations := nil;
  inherited;
end;

function TOperations.getOp( indx: Integer ): TOperation;
begin
  result := fOperations[indx];
end;

function TOperations.IndexOf(x, y: Integer): Integer;
begin
  result := 0;
  while ( result < fCount ) and not
    ( ( fOperations[result].x = x ) and
      ( fOperations[result].y = y ) ) do inc( result );
  if result = fCount then
    result := -1;
end;

procedure TOperations.Add( x, y: Integer; Value: TMinePixel; bCheck: Boolean = false );
begin
  if bCheck and ( IndexOf( x, y ) > -1 ) then
    exit;

  if fCapacity = fCount then
  begin
    fCapacity := fCapacity + fInc;
    SetLength( fOperations, fCapacity );
  end;

  fOperations[fCount].x := x;
  fOperations[fCount].y := y;
  fOperations[fCount].vTo := Value;
  Inc( fCount );
end;

procedure TOperations.Add2( x, y: Integer; ValueFrom, ValueTo: TMinePixel );
begin
  Add( x, y, ValueTo, false );
  fOperations[fCount-1].vFrom := ValueFrom;
end;

procedure TOperations.Apply( lmine: TMineContainer );
var
  i: Integer;
begin
  if fCount > 1 then
    QuickSort( fOperations, 0, fCount - 1 );

  for i := 0 to fCount - 1 do
  begin
    fOperations[i].vFrom := lMine.pixel[ fOperations[i].x, fOperations[i].y];
    lMine.setpixel( fOperations[i].x, fOperations[i].y , fOperations[i].vTo );
  end;

end;

procedure TOperations.Revert( lmine: TMineContainer );
var
  i: Integer;
begin
//  if fCount > 1 then
//    QuickSort( fOperations, 0, fCount - 1 );

//  for i := 0 to fCount - 1 do
  for i := fCount - 1 downto 0 do
  begin
    lMine.setpixel( fOperations[i].x, fOperations[i].y , fOperations[i].vFrom );
  end;

end;

procedure TOperations.RobotPos(var x, y: Integer);
var
  i: Integer;
begin
  i := 0;
  while ( i < fCount ) and ( fOperations[i].vFrom <> mpRobot ) do
    inc( i );
  if i < fCount then
  begin
    x := fOperations[i].x;
    y := fOperations[i].y;
  end else
  begin
    x := 0;
    y := 0;
  end;

end;

end.
