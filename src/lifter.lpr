program lifter;

{$mode objfpc}{$H+}

uses
    {$IFDEF UNIX}{$IFDEF UseCThreads}
    cthreads,
    {$ENDIF}{$ENDIF}
    Classes
  , SysUtils
  , uoperation
  , uengine
  , ugraph
  , umine
  , umineContainer
  , CustApp
  ;



procedure ReadFromStdIn( tsl: TStrings );
var
  st: Text;
  s: String;
begin
  Assign(st, '');
  reset(st);
  while not EOF(st) do
  begin
    readln(st, s);
    tsl.Add( s );
  end;
  Close(st);
end;

type
  { TLifter }

  TLifter = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    map: TMine;
    grp: TGraph;
    eng: TEngine;
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

{ TLifter }

procedure TLifter.DoRun;
var
  ErrorMsg: String;
  tsl: TStringList;
  i,j: Integer;
  s: String;
begin
  // quick check parameters
  ErrorMsg := CheckOptions('h','help');
  ErrorMsg := CheckOptions('d:','dump:');
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h','help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  { add your program here }
  tsl := TStringList.Create;
  if ParamCount > 0 then
  begin

    if Params[1] = 'dump' then
    begin
      if ParamCount <> 3 then
        ErrorMsg := Params[1] + ' needs 3 parameters, see --help'
      else
        tsl.LoadFromFile( Params[2] );
    end else

    if ( Params[1] = 'play' ) or ( Params[1] = 'view' ) or ( Params[1] = 'solve' ) then
    begin
      if ParamCount <> 2 then
        ErrorMsg := Params[1] + ' needs 2 parameters, see --help'
      else
        tsl.LoadFromFile( Params[2] );
    end else
      ErrorMsg := 'unknown param: ' + Params[1];

    if ErrorMsg <> '' then begin
      ShowException(Exception.Create(ErrorMsg));
      Terminate;
      Exit;
    end;

  end else
  begin
    // contest cofig.
    ReadFromStdIn( tsl );
  end;

  map := TMine.Create( tsl );
  tsl.Free;

  grp := TGraph.Create(map);
  eng := TEngine.Create(grp);

  if Params[1] = 'view' then
  begin
    map.PrintState;
  end else

  if Params[1] = 'dump' then
  begin
    s := Params[3];
    for i := 1 to Length( s ) do
    begin
      map.PrintState;
      map.DoMove( Str2Movement( s[i] ) );
      writeln( s[i] );
    end;
    map.PrintState;

    if map.done then
    begin
      writeln(cEndInfoStr[map.EndInfo]);
      writeln( 'total points = ' + IntToStr( map.Score ) );

    end;
  end else
  if Params[1] = 'solve' then
  begin
    eng.FindBruce;
    Writeln( eng.Cmd );

    writeln( 'total points = ' + IntToStr( eng.Score ) );
  end else
  begin
    eng.FindBruce;
    Writeln( eng.Cmd );
  end;

  eng.Free;
  grp.Free;
  map.Free;
  // stop program loop
  Terminate;
end;

constructor TLifter.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;

end;

destructor TLifter.Destroy;
begin
  inherited Destroy;
end;

procedure TLifter.WriteHelp;
begin
  { add your help code here }
  writeln('Usage: ',ExeName,' -h', ' :) uhhh' );
end;

var
  Application: TLifter;

{$R *.res}

begin
  Application:=TLifter.Create(nil);
  Application.Title:='Lifter';
  Application.Run;
  Application.Free;
end.

